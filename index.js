/* devel2go/test/js-testserver.js */

	var fs = require("fs");
	var http = require("http");
	var https = require("https");
	var path = require("path");
	var url = require("url");

	function onRequest(request, response)
	{
		var url_parts = url.parse(request.url, true);
		var pathname = url_parts.pathname;
		
		request.on("data", function(data)
		{
			body += data;
		});
		request.on("end", function()
		{
			if(request.method === "GET")
			{
				console.log("GET request for " + pathname);
				response.end('This is a test nodejs server. It is not secure!');
			}
			else if(request.method === "POST")
			{
				console.log("POST request for " + pathname);
				response.end('This is a test nodejs server. You have just posted ' + body);
			}
		});
	}

	function onSecureRequest(request, response)
	{
		var url_parts = url.parse(request.url, true);
		var pathname = url_parts.pathname;

		request.on("data", function(data)
		{
			body += data;
		});
		request.on("end", function()
		{
			if(request.method === "GET")
			{
				console.log("GET request for " + pathname);
				response.end('This is a test nodejs server. It is secure!');
			}
			else if(request.method === "POST")
			{
				console.log("POST request for " + pathname);
				response.end('This is a test nodejs server. You have just posted ' + body);
			}
		});
	}
	var portHTTPS = "8000"
	const args = process.argv;
	var dirPath = path.dirname(args[1]);
	//var portHTTP = args[2];
	//var portHTTPS = args[3];f
	if(args.length > 2) {
		portHTTPS = args[2];
	}
	console.log("Server dirPath: " + dirPath);
	//if(portHTTP) {
		//var serveHTTP = http.createServer(onRequest);
		//serveHTTP.listen(portHTTP);
		//console.log("Server has started on port: " + portHTTP);
	//}
	var fskey = fs.readFileSync(dirPath + "/keys/test-server.key");
	var fscert = fs.readFileSync(dirPath + "/keys/test-server.crt");
	var options = {
		key: fskey,
		cert: fscert,
	};
	var serveHTTPS = https.createServer(options, onSecureRequest);
	serveHTTPS.listen(portHTTPS);
	console.log(" NodeJS HTTPS server has started on port: " + portHTTPS);
	
	